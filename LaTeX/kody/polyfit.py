from __future__ import unicode_literals
import sys
import os.path
import numpy
from numpy.polynomial import Polynomial
from time import gmtime, strftime
import matplotlib

matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['text.latex.unicode'] = True
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import configparser
import string

error_message = ("Use parameters, for example:\n"
                 "\tpolyfit.exe x_axis_coordinates.txt y_axis_coordinates.txt degree_of_the_polynomial delimiter\n"
                 "where:\n"
                 "\tx_axis_coordinates.txt is file containing X axis coordinates separated by line break,\n"
                 "\ty_axis_coordinates.txt is file containing Y axis coordinates separated by line break,\n"
                 "\tdegree_of_the_polynomial means fitting polynomial counted by least squares polynomial count\n"
                 "\tdelimiter for each coordinates in files")


def main():
    config = configparser.ConfigParser()
    config.sections()
    try:
        config.read('config.cfg')
        x_coords_file = config['DEFAULT']['x_axis_coordinates']
        y_coords_file = config['DEFAULT']['y_axis_coordinates']
        delimiter = config['DEFAULT']['delimiter']
        degree = config['DEFAULT']['degree_of_the_polynomial']
        p0 = config['DEFAULT']['zero_point']
    except:
        handle_error(sys.exec_info()[0])

    try:
        try:
            x_coords = [float(line.rstrip(delimiter)) for line in open(x_coords_file)]
            y_coords = [float(line.rstrip(delimiter)) for line in open(y_coords_file)]
        except FileNotFoundError as e:
            handle_error(e)
        results = open('Results.txt', 'w')
        try:
            polynomial_fit = Polynomial.fit(x_coords, y_coords, int(degree), )
            '''''
            popt, pcov = curve_fit(function2, x_coords, y_coords, p0=(1, 1, 1, 1))
            results.write(
                '{0}+ a ={1}; b ={2}; c ={3}; d ={4}'.format(str(polynomial_fit), str(popt[0]), str(popt[1]), str(
                    popt[2]), str(popt[3])))
            '''''
            results.close()

            xx = numpy.linspace(0, 10000, 2000)
            # yy = function2(xx, *popt)
            yy = function2(xx, 1, 4.75, 1.01, 3.24)
            yy1 = function2(xx, 1, 5.01, 0.99, 2.25)
            yy2 = function2(xx, 1, 19.72, 1, 9.51)
            yy3 = function(xx, 4.55, 0.7866, 5.0613, 0.97)
            yy4 = function(xx, 5.4, 0.8141, 18.9375, 0.98)
            yy5 = function(xx, 4.64, 0.9159, 102.7411, 0.96)
            yy6 = function(xx, 4.85, 1.0196, 370.7263, 0.97)


            plt.xlabel('$K$')
            plt.ylabel(r'$R_k$')
            plt.title(r'Průběh koeficientu $R_K$')
            plt.xscale('log')
            plt.axis((10, 10000, 1, 3))
            plt.plot(x_coords, y_coords, 'bo', xx, yy, xx, yy1, xx, yy2, xx, yy3, xx, yy4, xx, yy5, xx, yy6)
            lgd = plt.legend(['Body', r'$\frac{L}{d} = 1$', r'$\frac{L}{d} = 2$', r'$\frac{L}{d} = 5$',
                              r'$\frac{L}{d} = 10$', r'$\frac{L}{d} = 25$', r'$\frac{L}{d} = 50$',
                              r"$\frac{L}{d} = 100$"],
                             loc='upper right')

            plt.savefig('plot.png', bbox_extra_artists=(lgd,), bbox_inches='tight')
        except UnboundLocalError as e:
            handle_error(e)
        except ValueError as e:
            handle_error(e)
        except:
            handle_error(sys.exec_info()[0])
        results.close()
    except IndexError as e:
        handle_error(e)


def function(x, a, b, c, d):
    return d + (a - d) / (1 + (x / c) ** b)


def function2(x, a, b, c, d):
    return (a * x + b) / (c * x + d)


def handle_error(e):
    if not os.path.isfile('Help.txt'):
        file = open('Help.txt', 'w')
        file.write(error_message)
        file.close()
    file = open('Error.txt', 'a')
    file.write("{0}\nFollowing error occurred:\n\t->{1}\nPlease, read Help.txt carefully!\n".format(
        strftime("%a, %d %b %Y %H:%M:%S", gmtime()), str(e)))
    file.close()


if __name__ == '__main__':
    main()
