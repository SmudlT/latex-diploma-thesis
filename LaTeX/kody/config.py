import configparser

config = configparser.RawConfigParser()

config['DEFAULT'] = {'x_axis_coordinates': 'x.txt',
                     'y_axis_coordinates': 'y.txt',
                     'delimiter': r'\n',
                     'degree_of_the_polynomial': '2',
                     'function': '(a*x+b)/(c*x+d)',
                     'zero_point': 'p0=(2, 1e-4, 2, 2)'}

with open('config.cfg', 'w') as configfile:
    config.write(configfile)
